package se325.lab04.concert.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import se325.lab04.concert.domain.Concert;


/**
 * Class to implement a simple REST Web service for managing Concerts.
 * <p>
 * ConcertResource implements a WEB service with the following interface:
 * - GET    <base-uri>/concerts/{id}
 * Retrieves a Concert based on its unique id. The HTTP response
 * message has a status code of either 200 or 404, depending on
 * whether the specified Concert is found.
 * <p>
 * - GET    <base-uri>/concerts?start&size
 * Retrieves a collection of Concerts, where the "start" query
 * parameter identifies an index position, and "size" represents the
 * max number of successive Concerts to return. The HTTP response
 * message returns 200.
 * <p>
 * - POST   <base-uri>/concerts
 * Creates a new Concert. The HTTP post message contains a
 * representation of the Concert to be created. The HTTP Response
 * message returns a Location header with the URI of the new Concert
 * and a status code of 201.
 * <p>
 * - DELETE <base-uri>/concerts
 * Deletes all Concerts, returning a status code of 204.
 * <p>
 * Where a HTTP request message doesn't contain a cookie named clientId
 * (Config.CLIENT_COOKIE), the Web service generates a new cookie, whose value
 * is a randomly generated UUID. The Web service returns the new cookie as part
 * of the HTTP response message.
 */

@Path("/concerts")
@Produces({
    MediaType.APPLICATION_XML,
    SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT
})
@Consumes({
    MediaType.APPLICATION_XML,
    SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT
})
public class ConcertResource {

    @GET
    @Path("{id}")
    @Produces
    public Response retrieveConcert(@PathParam("id") long id) {
        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();

        // Start a new transaction.
        em.getTransaction().begin();
    
        // Use the EntityManager to retrieve, persist or delete object(s).
        Concert concert = em.find(Concert.class, id);
        

        // Commit the transaction.
        em.getTransaction().commit();

        // When you're done using the EntityManager, close it to free up resources.
        em.close();

        if(concert == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(concert).build();
        }
    }

    @POST
    @Consumes
    public Response createConcert(Concert concert) {
       
        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();

        // Start a new transaction.
        em.getTransaction().begin();

        // Use the EntityManager to retrieve, persist or delete object(s).
        em.persist(concert);

        // Commit the transaction.
        em.getTransaction().commit();

        // When you're done using the EntityManager, close it to free up resources.
        em.close();


        return Response.created(URI.create("/concerts/" + concert.getId()))
                .build();
    }

    @PUT
    @Consumes
    public Response replaceConcert(Concert concert) {

        EntityManager em = PersistenceManager.instance().createEntityManager();

        // Start a new transaction.
        em.getTransaction().begin();

        // Use the EntityManager to retrieve, persist or delete object(s).
        Concert merged = em.merge(concert);

        // Commit the transaction.
        em.getTransaction().commit();

        // When you're done using the EntityManager, close it to free up resources.
        em.close();

        if(merged == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.noContent().build();
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes
    public Response deleteConcert(@PathParam("id") long id) {
        EntityManager em = PersistenceManager.instance().createEntityManager();

        // Start a new transaction.
        em.getTransaction().begin();

        // Use the EntityManager to retrieve, persist or delete object(s).
        Concert concert = em.find(Concert.class, id);
        if(concert != null) {
            em.remove(concert);
        }
        // Commit the transaction.
        em.getTransaction().commit();

        // When you're done using the EntityManager, close it to free up resources.
        em.close();

        if(concert == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.noContent().build();
        }
    }

    @DELETE
    public Response deleteAllConcerts() {
        EntityManager em = PersistenceManager.instance().createEntityManager();
        TypedQuery<Concert> concertQuery = em.createQuery("select c from Concert c", Concert.class);
        List<Concert> concerts = concertQuery.getResultList();

        // Start a new transaction.
        em.getTransaction().begin();

        // Use the EntityManager to retrieve, persist or delete object(s).
        for(Concert concert: concerts) {
            em.remove(concert);
        }

        // Commit the transaction.
        em.getTransaction().commit();

        // When you're done using the EntityManager, close it to free up resources.
        em.close();

       return Response.noContent().build();
    }
}
