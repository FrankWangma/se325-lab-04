package se325.lab04.concert.services;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/services")
public class ConcertApplication extends Application {

    private Set<Object> singletons = new HashSet<>();
    private final Set<Class<?>> classes = new HashSet<>();

    public ConcertApplication() {
        classes.add(ConcertResource.class);
        classes.add(SerializationMessageBodyReaderAndWriter.class);
    }


    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        PersistenceManager manager = new PersistenceManager();
        return singletons;
    }
}
